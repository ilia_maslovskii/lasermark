// For full API documentation, including code examples, visit https://wix.to/94BuAAs


import wixWindow from 'wix-window';
import wixPay from 'wix-pay';
import {sendNodeEmail} from 'backend/sendEmail';
import {createMyPayment} from 'backend/pay';


var customDrawing;

export function previewinput_change(event) {
  // send message to the HTML element
  $w("#html3").postMessage({message_preview: ($w("#previewinput").value)});
  // Set preview font 
  $w('#previewdisplay').html = '<h1 style="font-size: 5em; font-family:wf_8cc6558459d4401094a263f57;">'+$w("#previewinput").value+'</h1>';
}


export function selectSurfaceRepeater_itemReady($w, itemData, index) {
  	$w('#selectSurfaceButton').onClick(() => {
		// Select the choice using the selectChoiceForOption() function.
		selectImageChoiceForOption($w, 'surface', itemData);
	});
}

export function dropdownFontSize_change(event) {
  $w("#html3").postMessage({message_textsize: ($w("#dropdownFontSize").value)});
}

$w.onReady(() => {
	$w("#html3").onMessage((event) => {
			if (event.data){
				if (event.data.exportedDXF){
					//let receivedData = event.data.exportedDXF;
					customDrawing = event.data.exportedDXF;
					//sendNodeEmail(receivedData);
				}
			}
		});
});




export function selectFontRepeater_itemReady($w, itemData, index) {
  console.log(index + " " + itemData['title']);
	// Set the action that occurs when a user clicks a choice for the buttons option.
	$w('#selectFontButton').onClick(() => {
		// Select the choice using the selectChoiceForOption() function.
		selectChoiceForOption($w, itemData);
	});
}

// Set up each item in the pocket selection repeater as it is loaded.
export function pocketSelectionRepeater_itemReady($w, itemData, index) {

	// Set the action that occurs when a user clicks a choice for the pocket option.
	$w('#selectPocketButton').onClick(() => {
		// Select the choice using the selectChoiceForOption() function.
		selectChoiceForOption($w, 'pocket', itemData);
	});
}



function selectChoiceForOption($w, choiceData) {
 $w("#html3").postMessage({message_font: (choiceData.optionKey)});
}


function selectImageChoiceForOption($w, option, choiceData) {
	console.log(choiceData.title);
	// Change the image for the selected option to the selected choice's image.
	$w(`#${option}Img`).src = choiceData.displayImage;
	// Show the option image.
	$w(`#${option}Img`).show();
	// If a choice has been selected for all of the options:
  /*
	if (Object.keys(selectedOptions).length === NUMBER_OF_CHOICES) {
		//Enable the "Add to Cart" button.
		$w('#addToCartButton').enable();
	}
  */
}

export function orderButton_click(event, $w) {
  createMyPayment('33988a79-37f0-48b0-a7b5-dc7917457dae', $w('#quantityInput').value.valueOf())
    .then((payment) => {
      wixPay.startPayment(payment.id).then(function onPaymentResult (result) {
				if (result.status === "Successful"){
					console.log("Transaction succesfull, dispatching email...");
					sendNodeEmail(customDrawing, result);
				} else if (result.status === "Cancelled"){
					console.log("Transaction cancelled, dispathing testing email...");
					sendNodeEmail(customDrawing, result);
				} else if (result.status === "Failed"){
					console.log("Transaction failed, dispathing testing email...");
					sendNodeEmail(customDrawing, result);
				} else if (result.status === "Offline"){
					console.log("Transaction offline, dispathing testing email...");
					sendNodeEmail(customDrawing, result);
				} else if (result.status === "Undefined"){
					console.log("Transaction status undefined!");
				}
				
				//console.log("Transaction succesfull! " + JSON.stringify(result));
			}).catch("Transaction failed!");
    });
}

