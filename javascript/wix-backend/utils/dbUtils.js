import wixData from 'wix-data';
import {
    notFound,
    ok,
    serverError
} from 'wix-http-functions';

export function deleteExpired() {

let itemsToDelete;
let oneMonthAgoDate = new Date(
    new Date().getFullYear(),
    new Date().getMonth() - 1,
    new Date().getDate()
);
let returnoptions = {
    "headers": {
        "Content-Type": "application/json"
    }
};

let options = {
    "suppressAuth": true,
    "suppressHooks": true
};

wixData.query("customOrderSchematics")
  .lt("ttl", oneMonthAgoDate)
  .find(options)
  .then((results) =>{
       if (results.items.length > 0) {
            itemsToDelete = results.items;
            for (let i in itemsToDelete) {
                wixData.remove("customOrderSchematics", itemsToDelete[i]._id, options);
            }
            returnoptions.body = {
                "Status": "Items Deleted = " + results.items.length
            };
            return ok(returnoptions);
        }
        returnoptions.body = {
            "Status": "No Data To Delete"
        };
        return notFound(returnoptions);
    })
    // something went wrong
    .catch((error) => {
        returnoptions.body = {
            "Error": error
        };
        return serverError(returnoptions);
    });
}