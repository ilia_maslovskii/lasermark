var makerjs = require('makerjs');
var opentype = require('opentype.js');

var font = opentype.loadSync('./fonts/stardosstencil/StardosStencil-Regular.ttf');


function Demo(font, text, font_size, combine, center_character_origin) {

  this.models = {
    example: new makerjs.models.Text(font, text, font_size, combine, center_character_origin)
  };

  this.models.example.origin = [240,0];
}


var demo = new Demo(font, 'hello', 10, false, true);

/*
demo.metaParameters = [
  { title: "font", type: "font", value: "*" },
  { title: "text", type: "text", value: "Hello" },
  { title: "font size", type: "range", min: 10, max: 200, value: 72 },
  { title: "combine", type: "bool", value: false },
  { title: "center character origin", type: "bool", value: false }
];
*/
var svg = makerjs.exporter.toSVG(demo);

document.write(svg);