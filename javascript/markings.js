//renders a line and a circle

var makerjs = require('makerjs');

var line = { 
  type: 'line', 
  origin: [0, 0], 
  end: [50, 50] 
 };

var circle = { 
  type: 'circle', 
  origin: [0, 0],
  radius: 50
 };

var pathArray = [ line, circle ];

var svg = makerjs.exporter.toSVG(pathArray);

document.write(svg);

export function aFunction() {
    document.write(svg);
}